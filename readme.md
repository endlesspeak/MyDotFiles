## My Dot Files

### The very first
The repo contains my dotfiles (alacritty,i3-gaps,polybar,rofi,compton,etc)
Some of the dotfiles are out of date,which are renamed as 'archived'.But don't worry,when one of the dotfile are archived,there will be another newer file appear instead.


### Installation

Follow the instructions below to install the necessary software,some dependencies and some fonts acquired.

```shell
$ sudo pacman -S i3 
# Choose i3gaps in the following prompt,you can also type i3gaps directly.
$ sudo pacman -S alacritty gvim rofi 
# Terminal/vim/program starter,you may try dmenu as your program starter if you want.
$ sudo pacman -S cmake git python python2 pkg-config wireless_tools  
# Install the dependencies for building polybar
$ yay -S polybar
# It will be faster if you choose tuna's aur repo.
$ yay -S tty-unifont siji-git
# Install the fonts acquired
```
Next , clone the repo to your home directory , copy each folder with its subdirectory to the corresponding directory.
Pay attention please , dwm.desktop requires dynamic window manager. You need to build it first. See this [repo](https://gitee.com/endlesspeak/dwm) for my dwm builds and jump to dwm's [official website](https://suckless.org/) for details.
The following commands are optional,because you may not choose all the dotfiles to your .config folder.

```shell
$ git clone https://gitee.com/endlesspeak/MyDotFiles
$ cd MyDotFiles
$ cp -ri .config/ ~/.config
# Be careful,there are some archived files exist.
$ cp -ri .Scripts ~/.Scripts
$ cp .zshrc ~
$ sudo cp dwm.desktop /usr/share/xsessions/
```

As for zsh,install by yourself.Gitee provides a mirror of Oh-my-zsh for users to download.Clone it,edit the tools/install.sh so that you can build it locally for fear that downloading it from github.

### NeoVim

Forked from https://github.com/theniceboy/nvim
Other Reference: 
 - https://github.com/Innei/dotfiles

Edited by @EndlessPeak

#### Requirements

This nvim configuration REQUIRES NeoVim 0.5.0+,since manjaro linux **Do not** provides the version yet,you need to clone it and compile it by yourself. 

#### After Installation

- Install pip, and do `pip install --user pynvim`
- Install pip3, and do `pip3 install --user pynvim`
- Install node, and do `npm install -g neovim`
- Install nerd-fonts (actually it's optional but it looks real good)
- Install fzf by your package management.
- Install ag(the_silver_searcher) by your package management.
- Install figlet for inputing text ASCII art.
- Install xclip for system clipboard access. (Linux and xorg only)
- Do :checkhealth 
- Make sure your python path is correct,see `_machine_specific.vim` 

Documents about using method and key maps will be provided later.

### The way I learn Linux

The Linux expert,whose ID is **The CW**,is my teacher in this respect.(Though he is professional in medicine)

Under his guidance,I started to use i3wm as my major window manager. Surprisingly,I used i3wm for almost a month.

It would seem to me that i3wm is an excellent window manager without any doubt. At that time,both i3status and polybar are indispensable.I3status is be used for checking my internet connection,while polybar is just for creating an elegant appearance.

I forked another i3wm user's configuration files after two weeks. He extracted the fonts from MacOS,and drew some beautiful themes by himself. I have to admit that his i3wm is the prettiest one I have ever seen. It was 5 hours that I had spent over on it, though the result wasn't that satisfying. I strongly believe he spent far more time than me to make a serious of themes,which I appreciated quiet much.

His rofi was appended a kind of frosted glass on its appearance. Its blur seems to be so breathtaking that I just cannot forget. The stirring and different facade wholly bewitched me so that I kept using it until I changed to dwm. (BTW,Now I recompile the dwm files and start to use it again.)

I3wm finally comes to an end when The CW and Luke Smith both mention dwm. Dwm is an dynamic window manager for X,it manages windows in tiled, monocle and floating layouts. All of the layouts can be applied dynamically, optimising the environment for the application in use and the task performed.

It reads in its offical website that because dwm is customized through editing its source code, it's pointless to make binary packages of it. This keeps its user-base small and elitist. No novices asking stupid questions. 

>I don't know how you guys feel like,but I am sold. ——Luke Smith

It takes a lot of time to learn,config and try to use i3wm or dwm. I  was taught to use some valuable scripts written by bash shell so that it can help me meeting my desktop using requirement with less command. I highly recommend you read the scripts carefully, especially the ideology, the inner thoughts in it. Please note these scripts are mainly written by The CW, its license agreement is **MIT**.

Now,more and more people are using NeoVim as their default editor, and I decide to choose it too. That's the reason I won't spend lots of time to describe vim and its config files vimrc , though I used to spend over a week on configuring it. I am still learning and try to write the doc for NeoVim , but it will takes much much more time than I estimate.

If you are a new bud in linux , believe me, you will gain a lot from the videos upload by The CW and Luke Smith at BiliBili and YouToBe. After you finish your own configuration,you will feel a sense of satisfaction from the bottle of you heart.

May be I am truly a new bud in this respect,but since it will make me keep asking stupid questions,I cannot regard myself as a novice. Just figure all the difficulties out by myself,only in this way can my brain claim its price!

Keep fighting!

